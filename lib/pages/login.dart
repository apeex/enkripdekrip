import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:coding/util/function.dart';
import 'package:pointycastle/asymmetric/api.dart';
import 'package:coding/routes/approutes.dart';
import 'package:coding/validator/validform.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String username = '';
  String password = '';
  late RSAPublicKey serverPublicKey;
  final Book book = Book(); 
  String encryptedPassword2 = '';
  late final fernetKey;


  @override
  void initState() {
    super.initState();
    loadPublicKey();
    initializeFernetKey();
  }

  Future<void> initializeFernetKey() async {
    fernetKey = await book.getFernetKey(); 
  }

  Future<void> login() async {
    try {
      final response = await http.post(Uri.parse("http://192.168.1.11:5000/login"), body: {
        "Username": username,
        "Password": encryptedPassword2,
      });
      if (response.statusCode == 200) {
        final data = json.decode(response.body);

        print(data);
        
        // String? decryptedData = await book.decryptValue(data['token'], fernetKey);

        // print('decryptedData: $decryptedData');

        if (data['value'] == '1') {
          
          Navigator.of(context).pushReplacementNamed(AppRoutes.dashboard);

        } else {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: Text(data['message']),
              backgroundColor: Colors.red,
            ),
          );
        }        
      } else {
        print("Failed to send data. Status code: ${response.statusCode}");
      }
    } catch (e) {
      print('error: $e');
    }
  }

  Future<void> loadPublicKey() async {
    try {
      await book.loadPublicKey(); 
      setState(() {
        serverPublicKey = book.serverPublicKey;
      });
    } catch (e) {
      print('Error loading public key: $e');
    }
  }

  Future<void> encryptAndSendData() async {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();

      try {
        final encryptedPassword = await book.encryptData(password);
        setState(() {
          encryptedPassword2 = encryptedPassword;
        });
        await login();
      } catch (e) {
        print('Error encrypting and sending data: $e');
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Login Page'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Form(
          key: _formKey,
          autovalidateMode: AutovalidateMode.onUserInteraction,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              TextFormField(
                decoration: InputDecoration(
                  labelText: 'Email',
                  prefixIcon: Icon(Icons.email),
                ),
                keyboardType: TextInputType.emailAddress,
                validator: MyValidators.validateEmail,
                onSaved: (value) {
                  setState(() {
                    username = value!;
                  });
                },
              ),
              SizedBox(height: 20),
              TextFormField(
                decoration: InputDecoration(
                  labelText: 'Password',
                  prefixIcon: Icon(Icons.lock),
                ),
                obscureText: true,
                validator: MyValidators.validatePassword,
                onSaved: (value) {
                  setState(() {
                    password = value!;
                  });
                },
              ),
              SizedBox(height: 20),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  padding: EdgeInsets.symmetric(horizontal: 50, vertical: 15),
                ),
                onPressed: encryptAndSendData,
                child: Text(
                  'Login',
                  style: TextStyle(fontSize: 18),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

void main() => runApp(MaterialApp(home: Login()));
