class MyValidators {
  static String? validateName(String? value) {
    if (value == null || value.isEmpty) {
      return 'Please enter your name';
    } else if (value.length < 5) {
      return 'Name must be more than 5 characters';
    }
    return null;
  }

  static String? validatePassword(String? value) {
    if (value == null || value.isEmpty) {
      return 'Please enter a password';
    } else if (!isPasswordValid(value)) {
      return 'Password must have a combination of numbers and letters';
    }
    return null;
  }

  static String? validateEmail(String? value) {
    if (value == null || value.isEmpty) {
      return 'Please enter your email';
    } else if (!isEmailValid(value)) {
      return 'Email is not valid';
    }
    return null;
  }

  static bool isEmailValid(String email) {
    final emailRegex = RegExp(r'^[\w-]+(\.[\w-]+)*@[\w-]+(\.[\w-]+)+$');
    return emailRegex.hasMatch(email);
  }

  static bool isPasswordValid(String password) {
    final passwordRegex = RegExp(r'^(?=.*[0-9])(?=.*[a-zA-Z])');
    return passwordRegex.hasMatch(password);
  }
}