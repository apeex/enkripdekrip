import 'package:flutter/material.dart';
import 'package:coding/pages/login.dart';
import 'package:coding/pages/dashboard.dart';
import 'package:coding/pages/regis.dart';
import 'dart:io';
import 'package:coding/util/function.dart';
import 'package:coding/routes/approutes.dart';

void main() {
  HttpOverrides.global = MyHttpOverrides();
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback = (X509Certificate cert, String host, int port) => true;
  }
}

class _MyAppState extends State<MyApp> {
  Book book = Book();

  @override
  void initState() {
    super.initState();
    initializeApp();
  }

  Future initializeApp() async {
    await book.getdata();
    await book.getUID();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _checkIdInList(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return CircularProgressIndicator();
        } else {
          bool isIdInList = snapshot.data ?? false;
          String initialRoute2 = isIdInList ? AppRoutes.login : AppRoutes.regis;

          return MaterialApp(
            initialRoute: initialRoute2,
            routes: AppRoutes.routes,
          );
        }
      },
    );
  }

  Future<bool> _checkIdInList() async {
    await Future.delayed(Duration(seconds: 2));
    return book.listData.contains(book.id);
  }
}
