import 'package:flutter/material.dart';
import 'package:coding/pages/login.dart';
import 'package:coding/pages/dashboard.dart';
import 'package:coding/pages/regis.dart';

class AppRoutes {
  
  static const String login = '/login';
  static const String dashboard = '/dashboard';
  static const String regis = '/regis';

  static Map<String, WidgetBuilder> routes = {
    login: (context) => Login(),
    dashboard: (context) => Dashboard(),
    regis: (context) => Regis(),
  };
}