import 'package:encrypt/encrypt.dart';
import 'package:flutter_udid/flutter_udid.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:encrypt/encrypt.dart' as encrypt;
import 'package:pointycastle/asymmetric/api.dart';
import 'package:pointycastle/export.dart';
import 'package:pointycastle/pointycastle.dart';
import 'dart:typed_data';

class Book {
  String? _id;
  List listData = [];
  late RSAPublicKey serverPublicKey;

  String? get id => _id;

  Future<void> getUID() async {
    String udid = await FlutterUdid.udid;
    _id = udid;
  }

  Future<void> getdata() async {
    try {
      final response = await http.get(
          Uri.parse("http://192.168.1.11:5000/read"));
      if (response.statusCode == 200) {
        final List<Map<String, dynamic>> data = (json.decode(response.body) as List)
            .cast<Map<String, dynamic>>();
        List idList = data.map((item) => item['ID']).toList();
        listData = idList;
      } else {
        print("Error");
      }
    } catch (e) {
      print(e);
    }
  }

  final String publicKeyString = '''-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA1dJ8qZRL5qEVDBr+bXF6
zcmMq7bft2M81+WrGlSTU6oEHEYzko6gOrWZVs0agvEbd9/WKX33gjXk5kF3xknt
g3gakhdfbKXCDW41qnjcbjKxKHAHrM+/ocSHnjpZCu7dBPSwzeCSZezHbqkTlFC+
6bgfSOIrd99fvP2Ke9jQKTLk+MDQUfO2LMXyVpdZCJh41p4hIhYgkmu28fialMAn
F9DtrvCGvvK0Ijh+wVjghyvbsrFkg9wFEZjyb9HlEeTNkTjkkDFGTz+ryDqGmXQM
xJkQ9MQcs5HOFMky5aFLMx6t/M+Xyi1dLkNrkBq58ks+eS8NU5SsdB0p7AYo1Lef
0QIDAQAB
-----END PUBLIC KEY-----''';

  Future<void> loadPublicKey() async {
    try {
      serverPublicKey =
          RSAKeyParser().parse(publicKeyString) as RSAPublicKey;
      print('Public key loaded successfully');
    } catch (e) {
      print('Error loading public key: $e');
    }
  }

  // String hashPassword(String password) {
  //   final sha256 = SHA256Digest();
  //   final List<int> passwordBytes = utf8.encode(password);

  //   final Uint8List passwordUint8List = Uint8List.fromList(passwordBytes);

  //   final Uint8List hashedBytes = sha256.process(passwordUint8List);

  //   return base64Encode(hashedBytes);
  // }

  // Encrypt password
  Future<String> encryptData(String password) async {
    if (serverPublicKey == null) {
      throw Exception('Public key not loaded');
    }

    final hashedPassword = password;

    final encrypter = encrypt.Encrypter(encrypt.RSA(publicKey: serverPublicKey));
    final encrypted = encrypter.encrypt(hashedPassword);
    
    return encrypted.base64;
  }

  String decryptValue(String encryptedValue, String keyStringParameter) {
    print(keyStringParameter);
    final keyBytes = Uint8List.fromList(utf8.encode(keyStringParameter));
    final key = encrypt.Key.fromUtf8(base64UrlEncode(keyBytes));
    final fernet = encrypt.Fernet(key);
    final encrypter = encrypt.Encrypter(fernet);

    final decrypted = encrypter.decrypt(encrypt.Encrypted.fromBase64(encryptedValue));

    return decrypted;
  }



  Future<String> getFernetKey() async {
    final response = await http.get(
          Uri.parse("http://192.168.1.11:5000/get_key"));
    if (response.statusCode == 200) {
      final keyJson = json.decode(response.body);
      return keyJson['key'];
    } else {
      throw Exception('Failed to retrieve Fernet key');
    }
  }
  
}
