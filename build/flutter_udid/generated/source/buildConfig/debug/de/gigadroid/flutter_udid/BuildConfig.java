/**
 * Automatically generated file. DO NOT MODIFY
 */
package de.gigadroid.flutter_udid;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String LIBRARY_PACKAGE_NAME = "de.gigadroid.flutter_udid";
  public static final String BUILD_TYPE = "debug";
}
